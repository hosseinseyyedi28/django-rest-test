from rest_framework import serializers
from .models import Purchase
from django.contrib.auth.models import User
from rest_framework.serializers import ValidationError
import re


class PurchaseListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Purchase
        fields = '__all__'


class PurchaseDetailSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()

    class Meta:
        model = Purchase
        fields = ('id', 'user', 'purchase', 'name', 'date_time', 'phone_number', 'email', 'address')
        read_only_fields = []

    def validate(self, data):
        phone_number = data['phone_number']
        if not re.match(r'^(\+98|0)?9\d{9}$', phone_number):
            raise serializers.ValidationError('invalid phone_number')
        return data

    def get_user(self, obj):
        return str(obj.user.username)


class PurchaseCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Purchase
        fields = ('id', 'purchase', 'name', 'date_time', 'phone_number', 'email', 'address')


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('username', 'email', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def validate(self, data):
        email = data['email']
        user_qs = User.objects.filter(email=email)
        if user_qs.exists():
            raise ValidationError("This email has been registered.")
        return data

    def create(self, validated_data):
        user = User(
            username=validated_data['username'],
            email=validated_data['email'],
            password=validated_data['password']
        )
        user.set_password(validated_data['password'])
        user.save()
        return user

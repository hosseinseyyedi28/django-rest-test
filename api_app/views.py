from rest_framework.throttling import UserRateThrottle
from api_app.pagination import PurchaseLimitOffsetPagination
from .models import Purchase
from .serializers import PurchaseListSerializer, PurchaseDetailSerializer, PurchaseCreateSerializer, UserSerializer
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .permissions import IsOwnerOrReadOnly
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
)


class PurchaseListView(generics.ListAPIView):
    serializer_class = PurchaseListSerializer
    permission_classes = [IsAuthenticated]
    pagination_class = PurchaseLimitOffsetPagination

    def get_queryset(self):
        filters = {}
        if not self.request.user.is_superuser:
            filters['user'] = self.request.user

        queryset = Purchase.objects.filter(**filters)
        return queryset


class PurchaseDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Purchase.objects.all()
    serializer_class = PurchaseDetailSerializer
    lookup_field = 'pk'
    permission_classes = [IsOwnerOrReadOnly]

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)


class PurchaseCreateView(generics.CreateAPIView):
    queryset = Purchase.objects.all()
    serializer_class = PurchaseCreateSerializer
    permission_classes = [IsAuthenticated]
    throttle_classes = [UserRateThrottle]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class UserCreate(APIView):
    permission_classes = [AllowAny]
    serializer_class = UserSerializer

    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            if user:
                return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

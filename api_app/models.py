from django.db import models
from django.contrib.auth.models import User


class Purchase(models.Model):
    purchase = models.CharField(max_length=150)
    date_time = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=150)
    phone_number = models.CharField(max_length=20)
    email = models.EmailField(max_length=150)
    address = models.TextField()

    def __str__(self):
        return str(self.user)

from .views import UserCreate, PurchaseListView, PurchaseDetailAPIView, PurchaseCreateView
from django.urls import path
from rest_framework_jwt.views import obtain_jwt_token


urlpatterns = [
    path("register/", UserCreate.as_view(), name="user_create"),
    path("login/", obtain_jwt_token),
    path('purchase/<int:pk>/', PurchaseDetailAPIView.as_view(), name='purchase'),
    path('purchase/', PurchaseListView.as_view()),
    path('purchase/create', PurchaseCreateView.as_view(), name="create"),
]

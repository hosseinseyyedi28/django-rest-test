from rest_framework.pagination import LimitOffsetPagination


class PurchaseLimitOffsetPagination(LimitOffsetPagination):
    default_limit = 5
    max_limit = 5
 ## installation

```bash
$ git clone https://gitlab.com/hosseinseyyedi28/django-rest-test.git
$ cd django-rest-test
$ pip install -r requirements.txt
```

## Usage

```bash
$ python manage.py runserver
```

- After register and login put your token in header.

. API Endpoints

- /api/register/ (User registration endpoint)
- /api/login/ (User login endpoint)
- /api/purchase/ (User list of purchase)
- /api/purchase/id/ (Detail of purchase)
- /api/purchase/create/ (Create purchase list)
